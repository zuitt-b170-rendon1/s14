// JS Functions
/*
Functions are used to create reuseabke commands/statements that prevents the dev from typing a bunch of codes. In the field, a big number of lines of codes is the normal output; using functions in JS would save the dev a lot of time and effort
*/

function printStar(){

/*
	functions can also use paramenters. these parameters can be defined and a part of the  command inside the function. when called, parameters can be replaced with the target value of the developer. make sure that the value is inside quotations when called

	SYNTAX
	function functionName(parameters){
		command/statement
	}


*/	
	console.log("*")
};

printStar();
printStar();
printStar();
printStar();

function sayHello(name){
	console.log("Hello " + name)
};

sayHello("Carlo");

/*function alertPrint(){
	alert("Hello");
	console.log("Hello");
}

alertPrint();
*/

// Function that accepts two numbers and prints the sum
	function add(x,y){
		let sum = x + y;
		console.log(sum)
	}

add(1,2)
add(10,44)
// add(831, 440, 123); if the number of parameters defined exceeds the needed, the excess would ignored by JS

// Three parameters
// display the fname, lname, age
function printBio(fname, lname, age){
	// console.log("Hello " + fname + " " + lname + " " + age)

	// using template literals
	// backticks - the symbol on the left side of the 1 in the keyboard
	/*
	declared by the use of backticks with dollar sign and curly braces with the paremeters inside the braces
	*/
	console.log(`Hello ${fname} ${lname} ${age}`)
}

printBio("Carlo", "Rendon", 12);

function createFullname(lname, fname, mname) {
	// return specifies the value to be given by the function once it is finish executing. the value can be given to a variable. it only gives value, but does not display them in the console. that's why we also need to log the variable in the console, outside the function statement
	return `${fname} ${mname} ${lname}`

}

let fullname = createFullname("Cruz", "Juan", "Dela");
console.log(fullname);