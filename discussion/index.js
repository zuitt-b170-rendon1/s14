//one line comment (ctrl + slash)
/*
multiline comment (ctrl + shift + slash)
*/

console.log("Hello Batch 170!");

/*
javascript - we can see the messages/log in the console

browser consoles are part of the browsers which will allow us to see or log the messages, data or information from the programming language
	they are easily accessed in the dev tools

Statements
	instructions, expressions that we add to the programming language to communicate with the computers
usually ending with a semicolon (;). However, JS has implemented a way to automatically add semicolons at the end of the line

Syntax
	set of rules that describes how statements should be made/constructed

	lines/blocks of codes must follow a certain set of rules for it to work properly
*/


/*
console.log("Carlo Rendon");
console.log("adobo");
console.log("siomai");
console.log("lumpia");
*/


let	food1 = "adobo"



console.log(food1)


/*
Variable are way to store information or data within the JS
	to create a variable, we first declare the name of the variable with either let/const keyword

	let nameofVariable

	then we can initialize (define) the variable with a value or data
	
	let nameofVariable = <value>

*/

console.log("My favorite food is: " + food1);


let food2 = "kare kare";


console.log("My favorite food is: " + food1 + " and " + food2);

let food3;
/*
	we can create variables w/o values, but the variable's content would log undefined

*/

console.log(food3);

food3="chickenjoy"

/*
	we can update the content of a variable by reassigning the value using an assignment operator (=)

	assignment operator (=) let us (re)assign values to a variable

*/

console.log(food3)

/*
	we cannot create another variable with the same name. it will result into error

let food1 ="Flat Tops"

console.log(food1)
*/ 

/*
	const keyword will also allow creation of variable. However, with a const keyword, we create constant variable, which means the data saved in these variable will not change, cannot changed, and should not be changed.

*/

const pi= 3.1416;

console.log(pi);

/*
pi = "pizza";

console.log(pi);
*/

/*
	we also cannot create a const variable w/o initialization or assigning its value
const gravity;
console.log(gravity)*/

/*mini activity
LET KEYWORD
reassign a new value for food 1 with another favorite food
reassign a new value for food 2 with another favorite food
log the values of both variable in the console

CONST KEYWORD
create a const variable called sunriseDirection with East as its value
create a const variable called sunsetDirection with West as its value
log the values of both variable in the console
*/

food1 = "lumpia";
food2 = "sinigang";

console.log(food1);
console.log(food2);

const sunriseDirection ="East";
const sunsetDirection ="West";

console.log(sunriseDirection);
console.log(sunsetDirection);

/*
	Guideline in creating a JS variable
	1. We can create a let variable with the let keyword. let variables can be reassigned but not redeclared

	2. Creating a variable has two parts: Declaration of the variable and Initialization of the initial value of the variable through the use of assignment operator (=)

	3. A let variable can be declared w/o initialization. However, the  value of the variable will be undefined until it is re-assigned with a value

	4. Not defined vs Undefined. Not defined error means that the variable is used but NOT DECLARED. Undefined results from a variable that is used but NOT INITIALIZED.

	5. We can use const keyword to create variables. Constant variables cannot be declared w/o initialization. Constant variables cannot be reassigned.

	6. When creating variable names, start with small caps, this is because we are avoiding conflict with JS that is named with capital letters

	7. If the variable would need two words, the naming convention is the used of camelCase. Do not add any space/s for the variables with words as names.
*/


/*
//Data types

*/

/*
string
	are data types which are alphanumerical text. The could be a name, phrase, or even a sentence.We can create stringdata types with a single quote (') or with a double quote("). The text inside the quotation marks will be displayed as it is.

	keyword variable = "string data"
*/

console.log("Sample String Data");

/*
numberic data types
	are data types which are numeric. numeric data types are diplayed when numbers are not placed in the quotation marks

	keyword variable = numeric data;
*/
console.log(0123456789);
console.log(1+1)

// Mini activity
/*
	using string data type
		diplay in the console your:
		name (first and last)
		birthday
		province

	using numeric data type
		display in the contrl your:
		high score in the last game you played
		favorite number
		your favorite number in electric fan/aircon
*/

console.log("Carlo Rendon")
console.log("July 2, 1988")
console.log("Manila")

console.log(865)
console.log(7)
console.log(3)